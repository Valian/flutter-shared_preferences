import 'package:shared_preferences/shared_preferences.dart';

class PreferenciaUsuario{

static final PreferenciaUsuario _instancia = new PreferenciaUsuario._internal();


  factory PreferenciaUsuario(){
    return _instancia;
  }
  PreferenciaUsuario._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    _prefs = await SharedPreferences.getInstance();
  }



// bool _colorSecundario;
// int _genero;
// String _nombre;

//get y set del genero
get genero {
  return _prefs.getInt('genero') ?? 1;
}
set genero(int value){
  _prefs.setInt('genero', value);
}
//get y set del color secundario
get colorSecundario {
  return _prefs.getBool('colorSecundario') ?? false;
}
set colorSecundario(bool value){
  _prefs.setBool('colorSecundario', value);
}
//get y set del nombreUsuario
get nombreUsuario {
  return _prefs.getString('nombreUsuario') ?? '';
}
set nombreUsuario(String value){
  _prefs.setString('nombreUsuario', value);
}

//get y set del ultimaPagina
get ultimaPagina {
  return _prefs.getString('nombreUsuario') ?? 'home';
}
set ultimaPagina(String value){
  _prefs.setString('nombreUsuario', value);
}
}

